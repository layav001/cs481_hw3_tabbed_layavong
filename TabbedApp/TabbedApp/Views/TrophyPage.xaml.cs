﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using TabbedApp.Models;
using TabbedApp.Views;
using TabbedApp.ViewModels;

namespace TabbedApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TrophyPage : ContentPage
    {
        ItemsViewModel viewModel;

        public TrophyPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();
        }
        /*
        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Item;
            if (item == null)
                return;

            await Navigation.PushAsync(new TeamPage());

            // Manually deselect item.
            //ItemsListView.SelectedItem = null;
        }
        */
        async void AddItem_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new NavigationPage(new NewItemPage()));
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }

        async void BrowseItemsPage_Appearing(object sender, EventArgs e)
        {
            ContentPage content = BrowseItemsPage;
            await Task.Delay(1200);
            BrowseItemsPage.BackgroundColor = Color.White;
            comp.Source = ImageSource.FromFile("compound.png");

        }

        async void BrowseItemsPage_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = BrowseItemsPage;
            await Task.Delay(1200);
            BrowseItemsPage.BackgroundColor = Color.Red;
            comp.Source = ImageSource.FromFile("compound.png");
        }

        private void P1_Clicked(object sender, EventArgs e)
        {

            comp.Source = ImageSource.FromFile("group.jpg");
        }

        private void Slide_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            double val = e.NewValue;
            n.Opacity = val ;
        }
    }
}
