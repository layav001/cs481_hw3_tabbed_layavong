﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using TabbedApp.Models;
using TabbedApp.ViewModels;
using System.Threading.Tasks;

namespace TabbedApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TeamPage : ContentPage
    {
        ItemDetailViewModel viewModel;
        public TeamPage(ItemDetailViewModel viewModel)
        {
            InitializeComponent();

            BindingContext = this.viewModel = viewModel;
        }
        public TeamPage()
        {
            InitializeComponent();

            var item = new Item
            {
                Text = "Team Members:",
                Description = "This is an item description."
            };

            viewModel = new ItemDetailViewModel(item);
            BindingContext = viewModel;
        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {
            league.Source = ImageSource.FromFile("league1.png");
            
        }

        async void TeamPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(1200);
            await league.RotateTo(0, 250, null);
            league.Source = ImageSource.FromFile("leagueteam.png");

            await csgo.RotateTo(0, 250, null);
            csgo.Source = ImageSource.FromFile("csgo.jpg");


        }

        async void TeamPage_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = teamPage;
            await Task.Delay(1200);
            await league.RotateTo(100, 250, null);
            await csgo.RotateTo(100, 250, null);
        }

        private void Csgo_Clicked(object sender, EventArgs e)
        {
            csgo.Source = ImageSource.FromFile("csgo1.jpg");

        }
    }
}