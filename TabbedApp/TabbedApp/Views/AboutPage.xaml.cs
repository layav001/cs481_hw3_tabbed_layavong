﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Home!", "This is the home page", "Confirm");
            base.BackgroundColor = Color.White;

        }

        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Leaving!", "On to the next page", "Confirm");
            base.BackgroundColor = Color.Black;

        }
    }
}