﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace TabbedApp.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "Home";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://www.youtube.com/channel/UCnrX2_FoKieobtw19PiphDw/featured")));
        }

        public ICommand OpenWebCommand { get; }
    }
}